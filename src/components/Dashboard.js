import React from "react";
import logo from "../img/logo.png";
import "../components/Dashboard.css";
import circle from "../img/circle.png";
import man2 from "../img/man2.png";
import Card from "../img/Card.png";
import dot from "../img/dot.png";

function Dashboard() {
  return (
    <>
      <div className="row">
        <div className="head">
          <h4>Good Morning</h4>
          <img src={logo} alt="logo" height="30px" />
        </div>
      </div>

      <div className="row">
        <div className="col-md-4 col-12">
          <div className="Students">
            <h1>12000</h1>
            <h6>Registered Students</h6>
          </div>
        </div>
        <div className="col-md-4  col-12">
          <div className="Students">
            <h1>28</h1>
            <h6>Counselling Scheduled</h6>
          </div>
        </div>
        <div className="col-md-4  col-12">
          <div className="Students">
            <h1>920</h1>
            <h6>Internship Offered</h6>
          </div>
        </div>

        <div className="col-md-4  ">
          <div className="average">
            <h6>Assessment results on average</h6>
            <br></br>
            <img src={circle} alt="circle" width="70%" />
            <br></br>
            <ul className="list">
              <li>ideas and oppurtunities</li>
              <li>Resource</li>
              <li>Into action</li>
            </ul>
            <br></br>
            <img src={man2} alt="boy" className="man2" />
          </div>
        </div>

        <div className="col-md-8">
          <div className="comparative">
            <div className="cb">
              <h3>Comparative Bar</h3>
              <img src={dot} alt="" className="dot" />
            </div>

            <div className="chart1">
              <div className="numb1">
                <p>43%</p>
                <p>EnglishProficiency</p>
                <p>57%</p>
              </div>
              <label for="file"></label>
              <progress
                id="file"
                value="43"
                max="100"
                className="bar1"
              ></progress>
              <div className="planned">
                <p>planned </p>
                <p>Notplanned</p>
              </div>
            </div>

            <div className="chart2">
              <div className="numb2">
                <p>19%</p>
                <p>IT Skills</p>
                <p>81%</p>
              </div>
              <label for="file"></label>
              <progress
                id="file"
                value="19"
                max="100"
                className="bar2"
              ></progress>
              <div className="Documented">
                <p>Documented </p>
                <p>Not Documented</p>
              </div>
            </div>

            <div className="chart3">
              <div className="numb3">
                <p>68%</p>
                <p>Employability Factors</p>
                <p>32%</p>
              </div>
              <label for="file"></label>
              <progress
                id="file"
                value="68"
                max="100"
                className="bar3"
              ></progress>
              <div className="Designed">
                <p>Designed </p>
                <p>Not Designed</p>
              </div>
            </div>

            <div className="chart4">
              <div className="numb4">
                <p>94%</p>
                <p>Other Factors</p>
                <p>6%</p>
              </div>
              <label for="file"></label>
              <progress
                id="file"
                value="43"
                max="100"
                className="bar4"
              ></progress>
              <div className="Coded">
                <p>Coded </p>
                <p>Not Coded</p>
              </div>
            </div>
          </div>
          <img src={Card} alt="Card" className="Card" />
        </div>
      </div>
    </>
  );
}

export default Dashboard;
